UITS Docker Workshop
====================

This is the sample project used for the Docker workshop given by the UITS Infrastructure Application Development Team.

## Sections

### What is Docker?

### Install Docker

### Run a basic container

### Docker CLI basics

### Build an image

### Install Django

    python -m pip install Django
    python -m django --version

### Volumes

