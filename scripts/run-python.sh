#/bin/bash

# This script will run the default python 3.8 docker image

# See https://hub.docker.com/_/python

docker run \
    -it \
    --rm \
    -p 0.0.0.0:80:8000 \
    --name python38 \
    python:3.8 /bin/bash

