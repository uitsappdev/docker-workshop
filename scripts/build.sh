#!/bin/bash

docker build \
    --file Dockerfile \
    --tag uits/django:latest \
    .
