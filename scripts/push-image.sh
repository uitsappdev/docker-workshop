#!/bin/bash -e
#
# This script demonstrates the basic steps involved with pushing a docker
# image up to an AWS ECR Repository.  The environment variables here are
# not populated in this project, so this will not work as-is, but is included
# for demonstration purposes.
#

# What version of the AWS CLI do we have? certain cmds differs between 1 and 2
AWS_CLI_VERSION_STRING=$(aws --version)
if [[ $AWS_CLI_VERSION_STRING == *"aws-cli/2"* ]]; then
    AWS_CLI_VERSION=2
    # Disable output paging
    export AWS_PAGER=""
else
    AWS_CLI_VERSION=1
fi

# Log into AWS ECR
if [[ $AWS_CLI_VERSION == 1 ]]; then
    aws ecr get-login --region $AWS_REGION --no-include-email | bash
else
    aws ecr get-login-password | docker login --username AWS --password-stdin https://$ACCOUNT_NUMBER.dkr.ecr.$AWS_REGION.amazonaws.com
fi

# $ECR_URI must be provided by the environment somehow
DOCKER_REPO=$ECR_URI
DOCKER_PACKAGE="uits"
DOCKER_IMAGENAME="django"
DOCKER_TAG=$(date +'%Y-%m-%d.%H%M%S')

# Tag for environment

# The build.sh script will create a local docker image tagged uits/django:latest
# Here we take that image, and tag it again with the full URI of the ECR repo we created
# These will look something like: 1234567890.dkr.ecr.us-west-2.amazonaws.com/uits/django:latest
# We tag and push the same image with two tags, one with a datetime format, and one with
# latest. This will overwrite the existing image that is tagged as latest, but keep a history
# of the datetime tagged images.

# Push an django:latest image
# This tags an image like 1234567890.dkr.ecr.us-west-2.amazonaws.com/uits/django:latest
DOCKER_ECR_IMAGENAME="$DOCKER_REPO:latest"
docker tag "$DOCKER_PACKAGE/$DOCKER_IMAGENAME:latest" "$DOCKER_ECR_IMAGENAME"
docker push "$DOCKER_ECR_IMAGENAME"
docker rmi "$DOCKER_ECR_IMAGENAME"

# Push an django:datestring image
# This tags the same image as 1234567890.dkr.ecr.us-west-2.amazonaws.com/uits/django:2020-05-07.142305
DOCKER_ECR_IMAGENAME="$DOCKER_REPO:$DOCKER_TAG"
docker tag "$DOCKER_PACKAGE/$DOCKER_IMAGENAME:latest" "$DOCKER_ECR_IMAGENAME"
docker push "$DOCKER_ECR_IMAGENAME"
docker rmi "$DOCKER_ECR_IMAGENAME"
