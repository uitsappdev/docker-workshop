#!/bin/bash

docker run \
    -it \
    --rm \
    -p 0.0.0.0:80:8000 \
    -v /Users/mark/Repositories/docker-workshop/app:/usr/src/app \
    --name django \
    uits/django:latest
